plugins {
    alias(libs.plugins.kotlin.jvm) apply false
    alias(libs.plugins.kotlinter) apply false
    alias(libs.plugins.versions)
    alias(libs.plugins.dependency.analysis)
}

subprojects {
    plugins.withType<org.jetbrains.kotlin.gradle.plugin.KotlinBasePlugin> {
        configure<org.jetbrains.kotlin.gradle.dsl.KotlinBaseExtension> {
            jvmToolchain(21)
        }

        tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompilationTask<*>>().configureEach {
            compilerOptions {
                allWarningsAsErrors = true
            }
        }
    }
}

tasks.register<Delete>("clean") {
    delete(rootProject.layout.buildDirectory)
}

tasks.dependencyUpdates {
    val versionRegex = "^[0-9]+([\\.\\-][0-9]+)*?$".toRegex()
    fun String.isStable() = this matches versionRegex

    rejectVersionIf {
        currentVersion.isStable() && !candidate.version.isStable()
    }
}
