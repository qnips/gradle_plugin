#!/usr/bin/env kotlin

@file:DependsOn(
    "io.ktor:ktor-server-cio-jvm:2.3.12",
)

import io.ktor.http.HttpStatusCode
import io.ktor.server.application.call
import io.ktor.server.cio.CIO
import io.ktor.server.engine.embeddedServer
import io.ktor.server.request.httpMethod
import io.ktor.server.request.path
import io.ktor.server.response.respond
import io.ktor.server.routing.post
import io.ktor.server.routing.routing

embeddedServer(CIO, port = 8080) {
    routing {
        post("/dbapi/Update/Upload") {
            with(context.request) {
                println("=== Request (Start)")
                headers.forEach { key, values ->
                    values.forEach { value ->
                        println("[Header] $key: $value")
                    }
                }
                println("[Path]   ${httpMethod.value} ${path()}")
                queryParameters.forEach { key, value ->
                    println("[Query]  $key: $value")
                }
                println("=== Request (Stop)")
            }

            call.respond(HttpStatusCode.OK)
        }
    }
}.start(wait = true)
