plugins {
    `kotlin-dsl`
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.kotlinter)
    alias(libs.plugins.android.lint)
    alias(libs.plugins.plugin.publish)
}

val fixtureConfiguration = configurations.create("fixtureClasspath")

// Append any extra dependencies to the test fixtures via a custom configuration classpath. This
// allows us to apply additional plugins in a fixture while still leveraging dependency resolution
// and de-duplication semantics.
tasks.withType<PluginUnderTestMetadata>().configureEach {
    pluginClasspath.from(fixtureConfiguration)
}

dependencies {
    compileOnly(libs.android.gradle.api) // compileOnly to not force a specific AGP version

    implementation(libs.bundles.ktor)
    implementation(libs.kotlinx.coroutines.core)

    testImplementation(gradleTestKit())
    testImplementation(libs.bundles.junit.jupiter)
    testImplementation(libs.truth)
    testImplementation(libs.truth.testkit)

    lintChecks(libs.androidx.lint.gradle)

    "fixtureClasspath"(libs.android.gradle)
    "fixtureClasspath"(libs.kotlin.gradle)
}

version = "0.3.0"
group = "io.qnips.gradle"

java {
    withJavadocJar()
    withSourcesJar()
}

kotlin {
    explicitApi()
}

tasks.withType<Test>().configureEach {
    useJUnitPlatform()
}

gradlePlugin {
    website = "https://bitbucket.org/qnips/gradle_plugin"
    vcsUrl = "https://bitbucket.org/qnips/gradle_plugin"

    plugins {
        create("gradle-plugin") {
            id = "io.qnips.gradle"
            displayName = "qnips Gradle plugin"
            implementationClass = "io.qnips.gradle.Plugin"
            description = "A plugin for managing whitelabel apps that a released using a custom update mechanism"
            tags = listOf(
                "android",
                "convenience",
                "whitelabel",
            )
        }
    }
}
