package io.qnips.gradle

import java.util.stream.Stream
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsProvider

/**
 * This [ArgumentsProvider] returns a [Stream] that contains three different entries.
 *
 * Each [Arguments] contains two `arguments`:
 *  * `androidSnippet: String?` that defines the flavors and should be included in the `build.gradle.kts`
 *  * `variants: Array<String>` that defines the expected variants
 *
 * The [Stream] contains three different [Arguments]:
 *
 *  1. Zero `flavorDimension`s: `[]`
 *  1. One `flavorDimension` with three `productFlavors`: `[foo, bar, baz]`
 *  1. Two `flavorDimension`s with two `productFlavors` each: `[foo, bar] × [free, paid] = [fooFree, fooPaid, barFree, barPaid]`
 */
class FlavorProvider : ArgumentsProvider {
    override fun provideArguments(context: ExtensionContext): Stream<Arguments> = Stream.of(
        Arguments.of(
            null,
            arrayOf(
                "main",
            ),
        ),
        Arguments.of(
            """
                android {
                    flavorDimensions += "dimension"

                    productFlavors {
                        create("foo")
                        create("bar")
                        create("baz")
                    }
                }
            """.trimIndent(),
            arrayOf(
                "foo",
                "bar",
                "baz",
            ),
        ),
        Arguments.of(
            """
                android {
                    flavorDimensions += listOf(
                        "dimension1",
                        "dimension2",
                    )

                    productFlavors {
                        create("foo") { dimension = "dimension1" }
                        create("bar") { dimension = "dimension1" }

                        create("free") { dimension = "dimension2" }
                        create("paid") { dimension = "dimension2" }
                    }
                }
            """.trimIndent(),
            arrayOf(
                "fooFree",
                "fooPaid",
                "barFree",
                "barPaid",
            ),
        ),
    )
}
