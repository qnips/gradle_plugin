package io.qnips.gradle

import com.autonomousapps.kit.truth.TestKitTruth.Companion.assertThat
import java.io.File
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir

class PluginTest {
    @TempDir
    lateinit var testDir: File

    @Test
    fun `Android Gradle application plugin missing`() {
        val result = testDir.runBuild(
            buildGradleKts = """
                plugins {
                    id("io.qnips.gradle")
                }
            """.trimIndent(),
            fail = true,
        )

        assertThat(result).output().contains("The Android Gradle application plugin must be applied for the qnips plugin to be configured.")
    }
}
