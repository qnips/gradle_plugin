package io.qnips.gradle

import com.google.common.truth.StringSubject

internal fun StringSubject.hasTasks(
    bumpVersion: Boolean,
    validate: Boolean,
    release: Boolean,
    tag: Boolean,
    upload: Boolean,
    variants: Array<String>,
) {
    fun conditionalContains(condition: Boolean, string: CharSequence) {
        if (condition) {
            contains(string)
        } else {
            doesNotContain(string)
        }
    }

    conditionalContains(bumpVersion, "bumpVersion - ")

    if (variants.isNotEmpty()) {
        variants.map { it.capitalized() }.forEach {
            conditionalContains(release, "release$it - ")
            conditionalContains(tag, "tag$it")
            conditionalContains(upload, "upload$it")
        }
    } else {
        conditionalContains(release, "release - ")
        conditionalContains(tag, "tag")
        conditionalContains(upload, "upload")
    }

    conditionalContains(validate, "validateWorkspace")
}
