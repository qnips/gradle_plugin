package io.qnips.gradle

import com.autonomousapps.kit.truth.TestKitTruth.Companion.assertThat
import java.io.File
import org.junit.jupiter.api.io.TempDir
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ArgumentsSource

class FlavorsTest {
    @TempDir
    lateinit var testDir: File

    @ParameterizedTest(name = "no extension {1}")
    @ArgumentsSource(FlavorProvider::class)
    fun `no extension`(androidSnippet: String?, variants: Array<String>) {
        val result = testDir.runBuild(
            snippets = listOf(
                androidSnippet,
            ),
        )

        assertThat(result).output().hasTasks(
            bumpVersion = true,
            validate = true,
            release = false,
            tag = false,
            upload = false,
            variants = variants,
        )
    }

    @ParameterizedTest(name = "extension with upload {1}")
    @ArgumentsSource(FlavorProvider::class)
    fun `extension with upload`(androidSnippet: String?, variants: Array<String>) {
        val result = testDir.runBuild(
            snippets = listOf(
                androidSnippet,
                """
                    qnips {
                        upload {
                            postUrl = { "https://localhost" }
                            authorization = "password"
                        }
                    }
                """.trimIndent(),
            ),
        )

        assertThat(result).output().hasTasks(
            bumpVersion = true,
            validate = true,
            release = true,
            tag = false,
            upload = true,
            variants = variants,
        )
    }

    @ParameterizedTest(name = "extension with git {1}")
    @ArgumentsSource(FlavorProvider::class)
    fun `extension with git`(androidSnippet: String?, variants: Array<String>) {
        val result = testDir.runBuild(
            snippets = listOf(
                androidSnippet,
                """
                    qnips {
                        git {
                            name = "name"
                            email = "email"
                        }
                    }
                """.trimIndent(),
            ),
        )

        assertThat(result).output().hasTasks(
            bumpVersion = true,
            validate = true,
            release = false,
            tag = true,
            upload = false,
            variants = variants,
        )
    }

    @ParameterizedTest(name = "extension with upload and git {1}")
    @ArgumentsSource(FlavorProvider::class)
    fun `extension with git and upload`(androidSnippet: String?, variants: Array<String>) {
        val result = testDir.runBuild(
            snippets = listOf(
                androidSnippet,
                """
                    qnips {
                        upload {
                            postUrl = { "https://localhost" }
                            authorization = "password"
                        }

                        git {
                            name = "name"
                            email = "email"
                        }
                    }
                """.trimIndent(),
            ),
        )

        assertThat(result).output().hasTasks(
            bumpVersion = true,
            validate = true,
            release = true,
            tag = true,
            upload = true,
            variants = variants,
        )
    }
}
