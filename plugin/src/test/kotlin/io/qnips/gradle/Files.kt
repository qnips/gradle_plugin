package io.qnips.gradle

import java.io.File
import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner

internal fun File.runBuild(
    snippets: List<String?> = emptyList(),
    arguments: Array<String> = arrayOf(
        "tasks",
        "--all",
    ),
    fail: Boolean = false,
) = runBuild(
    buildGradleKts = buildList {
        add(BUILD_GRADLE_KTS)
        addAll(snippets.filterNotNull())
    }.joinToString("\n\n"),
    arguments = arguments,
    fail = fail,
)

internal fun File.runBuild(
    settingsGradleKts: String = SETTINGS_GRADLE_KTS,
    buildGradleKts: String,
    arguments: Array<String> = arrayOf(
        "tasks",
        "--all",
    ),
    fail: Boolean = false,
): BuildResult {
    resolve("settings.gradle.kts").writeText(settingsGradleKts)
    resolve("build.gradle.kts").writeText(buildGradleKts)

    return GradleRunner.create()
        .withGradleVersion("8.10.2")
        .withPluginClasspath()
        .withProjectDir(this)
        .let {
            if (arguments.isNotEmpty()) {
                it.withArguments(*arguments)
            } else {
                it
            }
        }
        .withDebug(true)
        .let {
            if (fail) {
                it.buildAndFail()
            } else {
                it.build()
            }
        }
}

private val SETTINGS_GRADLE_KTS = """
    pluginManagement {
        repositories {
            gradlePluginPortal()
            google()
            mavenCentral()
        }
    }

    dependencyResolutionManagement {
        repositoriesMode = RepositoriesMode.FAIL_ON_PROJECT_REPOS
        repositories {
            google()
            mavenCentral()
        }
    }
""".trimIndent()

private val BUILD_GRADLE_KTS = """
    plugins {
        id("com.android.application")
        id("org.jetbrains.kotlin.android")
        id("io.qnips.gradle")
    }

    android {
        namespace = "io.qnips.plugin.test"

        compileSdk = 31

        defaultConfig {
            versionCode = 1
            versionName = "1.0"

            targetSdk = 31
            minSdk = 31
        }

        buildFeatures {
            buildConfig = true
        }
    }
""".trimIndent()
