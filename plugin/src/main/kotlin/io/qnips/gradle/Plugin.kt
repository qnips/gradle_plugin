package io.qnips.gradle

import com.android.build.api.artifact.SingleArtifact
import com.android.build.api.variant.ApplicationAndroidComponentsExtension
import com.android.build.api.variant.VariantOutputConfiguration
import com.android.build.gradle.AppPlugin
import io.qnips.gradle.dsl.Extension
import io.qnips.gradle.dsl.letIfEnabled
import io.qnips.gradle.tasks.BumpVersion
import io.qnips.gradle.tasks.ReadPrompt
import io.qnips.gradle.tasks.Release
import io.qnips.gradle.tasks.Tag
import io.qnips.gradle.tasks.Upload
import io.qnips.gradle.tasks.Validate
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.assign
import org.gradle.kotlin.dsl.configure
import org.gradle.kotlin.dsl.create
import org.gradle.kotlin.dsl.getByType
import org.gradle.kotlin.dsl.hasPlugin
import org.gradle.kotlin.dsl.register
import org.gradle.kotlin.dsl.withType

public class Plugin : Plugin<Project> {
    override fun apply(project: Project): Unit = with(project) {
        extensions.create<Extension>("qnips")

        plugins.withType<AppPlugin> {
            applyInternal()
        }

        afterEvaluate {
            require(plugins.hasPlugin(AppPlugin::class)) {
                "The Android Gradle application plugin must be applied for the qnips plugin to be configured."
            }
        }
    }

    private fun Project.applyInternal() {
        val extension = extensions.getByType<Extension>()

        val validateWorkspaceTask = tasks.register<Validate>(
            name = "validateWorkspace",
            extension.validateWorkspace,
            rootDir,
        ) {
            dryRun = extension.dryRun
        }

        val readPromptTask = tasks.register<ReadPrompt>(
            name = "readPrompt",
        ) {
            dependsOn(validateWorkspaceTask)
        }

        tasks.register<BumpVersion>(
            name = "bumpVersion",
            extension.git,
        )

        // https://medium.com/androiddevelopers/new-apis-in-the-android-gradle-plugin-f5325742e614
        // https://github.com/android/gradle-recipes
        extensions.configure<ApplicationAndroidComponentsExtension> {
            onVariants(selector().withBuildType("release")) { variant ->
                val flavorName = variant.flavorName.orEmpty()
                val flavorCap = flavorName.ifEmpty { "main" }.capitalized()

                val mainOutput = variant.outputs.single {
                    it.outputType == VariantOutputConfiguration.OutputType.SINGLE
                }

                val buildConfigFields = variant.buildConfigFields
                val uploadTask = extension.upload.letIfEnabled { uploadConfig ->
                    tasks.register<Upload>(
                        name = "upload$flavorCap",
                        uploadConfig,
                        flavorName,
                    ) {
                        brandHeader = buildConfigFields.getSanitized("APP_BRAND")
                        applicationId = variant.applicationId
                        apkDirectory = variant.artifacts.get(SingleArtifact.APK)
                        builtArtifactsLoader = variant.artifacts.getBuiltArtifactsLoader()
                        dryRun = extension.dryRun

                        mustRunAfter(validateWorkspaceTask)
                        mustRunAfter(readPromptTask)
                    }
                }

                val tagTask = extension.git.letIfEnabled { gitConfig ->
                    tasks.register<Tag>(
                        name = "tag$flavorCap",
                        gitConfig,
                        flavorName,
                        rootDir,
                    ) {
                        versionCode = mainOutput.versionCode
                        versionName = mainOutput.versionName
                        dryRun = extension.dryRun

                        if (uploadTask != null) {
                            mustRunAfter(uploadTask)
                        }
                    }
                }

                val releaseTask = uploadTask?.let {
                    tasks.register<Release>(
                        name = "release$flavorCap",
                        flavorName,
                    ) {
                        // Yeah, for some obscure-for-now reasons we can't use `taskProvider.configure { … }` here.
                        @Suppress("EagerGradleConfiguration")
                        uploadTask.get().updateChannel = this@register.updateChannel
                        @Suppress("EagerGradleConfiguration")
                        tagTask?.get()?.updateChannel = this@register.updateChannel

                        dependsOn(validateWorkspaceTask)
                        dependsOn(readPromptTask)
                        dependsOn(uploadTask)
                        if (tagTask != null) {
                            dependsOn(tagTask)
                        }
                    }
                }

                extension.hook.orNull?.invoke(tasks, flavorName, uploadTask, tagTask, releaseTask)
            }
        }
    }
}
