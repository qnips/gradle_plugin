package io.qnips.gradle

import io.qnips.gradle.dsl.Git
import java.io.File

internal fun File.executeGit(config: Git, vararg command: String) = execute(
    "git",
    "-c",
    "user.name='${config.name.get()}'",
    "-c",
    "user.email='${config.email.get()}'",
    *command,
)

internal fun File.execute(vararg command: String): String {
    val process = ProcessBuilder(*command)
        .directory(this)
        .start()
    process.waitFor()

    if (process.exitValue() != 0) {
        throw RuntimeException(process.errorStream.reader().use { it.readText() })
    }

    return process.inputStream.reader().use { it.readText() }
}
