package io.qnips.gradle

import com.android.build.api.variant.BuildConfigField
import org.gradle.api.provider.MapProperty

internal const val EMPTY = "§EMPTY§"

internal fun MapProperty<String, BuildConfigField<*>>.getSanitized(name: String) =
    getting(name).map { (it.value as? String).takeUnless { it == "null" }?.trim('"') ?: EMPTY }
