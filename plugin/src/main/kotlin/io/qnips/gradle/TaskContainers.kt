package io.qnips.gradle

import org.gradle.api.Action
import org.gradle.api.Task
import org.gradle.api.tasks.TaskContainer

internal inline fun <reified T : Task> TaskContainer.register(
    name: String,
    vararg constructorArgs: Any,
    configurationAction: Action<in T>? = null,
) = register(name, T::class.java, *constructorArgs).apply {
    configurationAction?.let { configure(it) }
}
