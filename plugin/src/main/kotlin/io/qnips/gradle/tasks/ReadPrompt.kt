@file:Suppress("InternalGradleApiUsage")

package io.qnips.gradle.tasks

import org.gradle.api.DefaultTask
import org.gradle.api.internal.tasks.userinput.UserInputHandler
import org.gradle.api.tasks.TaskAction
import org.gradle.kotlin.dsl.support.get

public open class ReadPrompt : DefaultTask() {
    @TaskAction
    public fun readPrompt() {
        val userInputHandler = services.get<UserInputHandler>()

        while (userInputHandler.askYesNoQuestion("Are you sure you execute the proper tasks?") != true) {
            // Repeat
        }
    }
}
