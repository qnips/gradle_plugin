package io.qnips.gradle.tasks

import javax.inject.Inject
import org.gradle.api.DefaultTask
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.options.Option

@Suppress("ktlint:standard:annotation", "ktlint:standard:indent")
public abstract class Release @Inject constructor(
    flavorName: String,
) : DefaultTask() {
    @get:Option(option = "channel", description = "One of [Testing, RC, Release]")
    @get:Internal
    public abstract val updateChannel: Property<UpdateChannel>

    init {
        description = buildString {
            append("Creates, uploads and tags a release")

            if (flavorName != "main") {
                append(" for flavor '$flavorName'")
            }

            append(" in the Testing channel.")
        }
        group = PLUGIN_GROUP
    }
}
