package io.qnips.gradle.tasks

import io.qnips.gradle.capitalized
import io.qnips.gradle.dsl.Git
import io.qnips.gradle.executeGit
import java.io.File
import javax.inject.Inject
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.options.Option

@Suppress("ktlint:standard:annotation", "ktlint:standard:indent")
public open class BumpVersion @Inject constructor(
    private val config: Git,
) : DefaultTask() {
    @Option(option = "semver", description = "Bumps a specific SemVer component ('major', 'minor', 'patch', 'build'")
    @Internal
    public lateinit var semVer: SemVer

    @Option(option = "scheme", description = "New version in scheme '{major}.{minor}.{patch}[+{build}]")
    @Internal
    public lateinit var scheme: String

    @InputFile
    public val buildGradleKtsIn: File = project.file("build.gradle.kts")

    @OutputFile
    public val buildGradleKtsOut: File = project.file("build.gradle.kts")

    @InputDirectory
    public val rootDir: File = project.rootDir

    @Input
    public val projectPath: String = project.path

    init {
        description = "Bumps the version."
        group = PLUGIN_GROUP
    }

    @TaskAction
    public fun bumpVersion() {
        when {
            this::semVer.isInitialized -> bumpSemVer()
            this::scheme.isInitialized -> bumpScheme()
            else -> throw GradleException(
                "Pass either '--semver=[major,minor,patch,build]' or '--scheme {major}.{minor}.{patch}[+{build}]'.",
            )
        }
    }

    private fun bumpSemVer() {
        val buildGradleKtsInText = buildGradleKtsIn.readText()

        fun SemVer.parse(): Int? {
            val regex = """val version${name.capitalized()} = (\d+)""".toRegex()
            val match = regex.find(buildGradleKtsInText) ?: return null

            val (version) = match.destructured
            return version.toInt()
        }

        val major = checkNotNull(SemVer.major.parse())
        val minor = checkNotNull(SemVer.minor.parse())
        val patch = checkNotNull(SemVer.patch.parse())
        val build = SemVer.build.parse()

        when (semVer) {
            SemVer.major -> updateVersion(major + 1, minor = 0, patch = 0, build = 0.takeIf { build != null })
            SemVer.minor -> updateVersion(major, minor + 1, patch = 0, build = 0.takeIf { build != null })
            SemVer.patch -> updateVersion(major, minor, patch + 1, build = 0.takeIf { build != null })
            SemVer.build -> updateVersion(major, minor, patch, checkNotNull(build) + 1)
        }
    }

    private fun bumpScheme() {
        logger.warn("'scheme' is discouraged for most of the bumps in favor of the new `--semver` option.")

        val regex = """^(\d+)\.(\d+)\.(\d+)(?:\+(\d+))?$""".toRegex()
        val match = regex.matchEntire(scheme)
            ?: throw GradleException("Wrong version scheme specified. Use '--scheme {major}.{minor}.{patch}[+{build}]'.")

        val (major, minor, patch, build) = match.destructured
        updateVersion(major.toInt(), minor.toInt(), patch.toInt(), build.ifEmpty { null }?.toInt())
    }

    private fun updateVersion(major: Int, minor: Int, patch: Int, build: Int?) {
        val components = listOfNotNull(
            "major" to major,
            "minor" to minor,
            "patch" to patch,
            "build" to build,
        )

        buildGradleKtsOut.writeText(
            components.fold(buildGradleKtsIn.readText()) { acc, (component, version) ->
                val field = "version${component.capitalized()}"
                acc.replace("""val $field = (\d+)""".toRegex(), "val $field = ${version ?: 0}")
            },
        )

        if (config.isEnabled) {
            val buildGradleKtsPath = buildGradleKtsOut.toRelativeString(rootDir)
            try {
                val readableVersion = buildString {
                    append("$major.$minor.$patch")

                    if (build != null) {
                        append(" Build $build")
                    }
                }

                rootDir.executeGit(
                    config = config,
                    "commit",
                    buildGradleKtsPath,
                    "-m",
                    "Bump version to $readableVersion ($projectPath)",
                )

                logger.error("Bumped version to $readableVersion")
            } catch (e: Exception) {
                logger.error("Could not commit changes. Check & commit $buildGradleKtsPath manually.", e)
            }
        } else {
            logger.warn("Skipped committing the changes. Commit manually!")
        }
    }

    @Suppress("EnumEntryName")
    public enum class SemVer {
        major,
        minor,
        patch,
        build,
    }
}
