package io.qnips.gradle.tasks

import com.android.build.api.variant.BuiltArtifactsLoader
import io.ktor.client.HttpClient
import io.ktor.client.engine.java.Java
import io.ktor.client.request.forms.MultiPartFormDataContent
import io.ktor.client.request.forms.formData
import io.ktor.client.request.headers
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.client.request.url
import io.ktor.client.statement.bodyAsText
import io.ktor.client.utils.buildHeaders
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpStatusCode
import io.ktor.http.formUrlEncode
import io.ktor.utils.io.streams.asInput
import io.qnips.gradle.EMPTY
import io.qnips.gradle.dsl.Upload
import java.io.File
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.gradle.api.DefaultTask
import org.gradle.api.InvalidUserDataException
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.options.Option

@Suppress("ktlint:standard:annotation")
public abstract class Upload @Inject constructor(
    private val config: Upload,
    private val flavorName: String,
) : DefaultTask() {
    @get:Option(option = "channel", description = "One of [Testing, RC, Release]")
    @get:Internal
    public abstract val updateChannel: Property<UpdateChannel>

    @get:Input
    public abstract val brandHeader: Property<String>

    @get:Input
    public abstract val applicationId: Property<String>

    @get:InputFiles
    public abstract val apkDirectory: DirectoryProperty

    @get:Internal
    public abstract val builtArtifactsLoader: Property<BuiltArtifactsLoader>

    @get:Optional
    @get:Input
    public abstract val dryRun: Property<Boolean>

    @TaskAction
    public fun upload() {
        val builtArtifacts = requireNotNull(builtArtifactsLoader.get().load(apkDirectory.get())) {
            "Cannot load APKs!"
        }
        val singleBuiltArtifact = requireNotNull(builtArtifacts.elements.singleOrNull()) {
            "Expected exactly one APK, got ${builtArtifacts.elements.size}!"
        }

        val client = HttpClient(Java) {
            expectSuccess = false
        }
        val queryArguments = listOf(
            "applicationId" to applicationId.get(),
            "versionCode" to singleBuiltArtifact.versionCode?.toString(),
            "versionName" to singleBuiltArtifact.versionName,
            "updateChannel" to updateChannel.getOrElse(UpdateChannel.Testing).ordinal.toString(),
        )

        val url = "${config.postUrl.get().invoke(flavorName)}?${queryArguments.formUrlEncode()}"
        if (dryRun.orNull == true) {
            Result.success("[Upload skipped: $url]")
        } else {
            runBlocking(Dispatchers.IO) {
                runCatching {
                    val apkFile = File(singleBuiltArtifact.outputFile)

                    val response = client.post {
                        url(url)
                        headers {
                            append(HttpHeaders.Authorization, config.authorization.get())

                            if (brandHeader.isPresent && brandHeader.get() != EMPTY) {
                                append("App-Brand", brandHeader.get().trim('"'))
                            }
                        }
                        setBody(
                            MultiPartFormDataContent(
                                parts = formData {
                                    appendInput(
                                        key = "apk",
                                        headers = buildHeaders {
                                            append(HttpHeaders.ContentDisposition, "filename=app.apk")
                                            append(HttpHeaders.ContentType, "application/vnd.android.package-archive")
                                        },
                                        size = apkFile.length(),
                                    ) {
                                        apkFile.inputStream().asInput()
                                    }
                                },
                            ),
                        )
                    }

                    val status = response.status
                    val body = response.bodyAsText().trim('"')

                    if (status == HttpStatusCode.OK) {
                        body
                    } else {
                        throw Exception("$status $body")
                    }
                }
            }
        }.onSuccess {
            logger.error("Update: ✔ $it")
        }.onFailure {
            throw InvalidUserDataException("Update: ✖ ${it.message}", it)
        }
    }
}
