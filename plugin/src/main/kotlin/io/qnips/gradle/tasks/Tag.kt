package io.qnips.gradle.tasks

import io.qnips.gradle.dsl.Git
import io.qnips.gradle.executeGit
import java.io.File
import javax.inject.Inject
import org.gradle.api.DefaultTask
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.options.Option

@Suppress("ktlint:standard:annotation")
public abstract class Tag @Inject constructor(
    private val config: Git,
    private val flavorName: String,
    private val rootDir: File,
) : DefaultTask() {
    @get:Option(option = "channel", description = "One of [Testing, RC, Release]")
    @get:Internal
    public abstract val updateChannel: Property<UpdateChannel>

    @get:Input
    public abstract val versionCode: Property<Int>

    @get:Input
    public abstract val versionName: Property<String>

    @get:Optional
    @get:Input
    public abstract val dryRun: Property<Boolean>

    @TaskAction
    public fun tag() {
        val tag = buildString {
            if (config.tagPrefix.isPresent) {
                append(config.tagPrefix.get()).append('/')
            }

            if (flavorName.isNotEmpty()) {
                append(flavorName).append('/')
            }

            append("${versionCode.get()}_${versionName.get()}")
        }
        val message = buildString {
            append("VersionCode ${versionCode.get()}")

            if (flavorName.isNotEmpty()) {
                append(" ($flavorName)")
            }

            append(" [${updateChannel.getOrElse(UpdateChannel.Testing)}]")
        }

        if (dryRun.orNull == true) {
            logger.error("[Git Tag skipped: $tag $message")
        } else {
            rootDir.executeGit(
                config = config,
                "tag",
                "-a",
                "-m",
                message,
                tag,
            )

            logger.error("Git Tag added: $tag")
        }
    }
}
