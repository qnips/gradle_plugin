package io.qnips.gradle.tasks

public enum class UpdateChannel {
    Testing,
    RC,
    Release,
}
