package io.qnips.gradle.tasks

import io.qnips.gradle.dsl.ValidateWorkspace
import io.qnips.gradle.dsl.ValidationError
import io.qnips.gradle.execute
import java.io.File
import javax.inject.Inject
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction

@Suppress("ktlint:standard:annotation")
public abstract class Validate @Inject constructor(
    private val config: ValidateWorkspace,
    private val rootDir: File,
) : DefaultTask() {
    @get:Optional
    @get:Input
    public abstract val dryRun: Property<Boolean>

    @TaskAction
    public fun validateWorkspace() {
        val branch = rootDir.execute(
            "git",
            "rev-parse",
            "--abbrev-ref",
            "HEAD",
        ).trim()

        if (branch !in VALID_BRANCHES) {
            return onError { "Your current branch '$branch' is not valid\nAllowed branches: ${VALID_BRANCHES.joinToString()}" }
        }

        try {
            rootDir.execute(
                "git",
                "diff-index",
                "--quiet",
                "HEAD",
            )
        } catch (_: Exception) {
            return onError { "You have uncommited changes" }
        }

        // Get latest changes from repository
        rootDir.execute(
            "git",
            "fetch",
            "origin",
        )

        try {
            rootDir.execute(
                "git",
                "diff",
                "--quiet",
                "origin/$branch",
            )
        } catch (_: Exception) {
            return onError { "Your current branch '$branch' is not identical with 'origin/$branch'" }
        }
    }

    private fun onError(message: () -> String) {
        val onError = if (dryRun.orNull == true) {
            ValidationError.LOG
        } else {
            config.onError.orNull
        }
        when (onError) {
            ValidationError.FAIL -> throw GradleException(message())
            ValidationError.LOG -> logger.error(message())
            ValidationError.IGNORE -> Unit
            null -> Unit
        }
    }

    private companion object {
        private val VALID_BRANCHES = arrayOf(
            "dev",
            "master",
            "main",
        )
    }
}
