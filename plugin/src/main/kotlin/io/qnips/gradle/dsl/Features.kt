package io.qnips.gradle.dsl

internal fun <T : Feature, R> T.letIfEnabled(block: (T) -> R) = takeIf { isEnabled }?.let(block)
