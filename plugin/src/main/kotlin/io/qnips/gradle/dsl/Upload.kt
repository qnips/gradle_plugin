package io.qnips.gradle.dsl

import org.gradle.api.provider.Property

public abstract class Upload : Feature() {
    public abstract val postUrl: Property<(flavorName: String?) -> String>
    public abstract val authorization: Property<String>

    override val isEnabled: Boolean
        get() = postUrl.isPresent &&
            authorization.isPresent
}
