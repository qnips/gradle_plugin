package io.qnips.gradle.dsl

import org.gradle.api.provider.Property
import org.gradle.kotlin.dsl.assign

public abstract class ValidateWorkspace {
    public abstract val onError: Property<ValidationError>

    public fun fail() {
        onError = ValidationError.FAIL
    }

    public fun log() {
        onError = ValidationError.LOG
    }

    public fun ignore() {
        onError = ValidationError.IGNORE
    }
}
