package io.qnips.gradle.dsl

public enum class ValidationError {
    FAIL,
    LOG,
    IGNORE,
}
