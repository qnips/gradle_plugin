package io.qnips.gradle.dsl

import io.qnips.gradle.tasks.Release
import io.qnips.gradle.tasks.Tag
import io.qnips.gradle.tasks.Upload as UploadTask
import javax.inject.Inject
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.TaskProvider
import org.gradle.kotlin.dsl.newInstance

public typealias Hook = (
    tasks: TaskContainer,
    flavorName: String,
    uploadTask: TaskProvider<UploadTask>?,
    tagTask: TaskProvider<Tag>?,
    releaseTask: TaskProvider<Release>?,
) -> Unit

@Suppress("ktlint:standard:annotation")
public abstract class Extension @Inject constructor(
    objects: ObjectFactory,
) {
    internal val validateWorkspace: ValidateWorkspace = objects.newInstance()

    public fun validateWorkspace(action: ValidateWorkspace.() -> Unit) {
        action(validateWorkspace)
    }

    internal val upload: Upload = objects.newInstance()

    public fun upload(action: Upload.() -> Unit) {
        action(upload)
    }

    internal val git: Git = objects.newInstance()

    public fun git(action: Git.() -> Unit) {
        action(git)
    }

    public abstract val hook: Property<Hook>

    public abstract val dryRun: Property<Boolean>
}
