package io.qnips.gradle.dsl

import org.gradle.api.provider.Property

public abstract class Git : Feature() {
    public abstract val name: Property<String>
    public abstract val email: Property<String>

    public abstract val tagPrefix: Property<String>

    override val isEnabled: Boolean
        get() = name.isPresent &&
            email.isPresent
}
