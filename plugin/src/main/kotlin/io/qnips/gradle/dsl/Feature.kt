package io.qnips.gradle.dsl

public abstract class Feature {
    internal abstract val isEnabled: Boolean
}
