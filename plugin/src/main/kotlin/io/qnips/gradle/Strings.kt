package io.qnips.gradle

internal fun String.capitalized() = replaceFirstChar(Char::titlecase)
